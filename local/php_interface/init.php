<?
use Bitrix\Main\Context;
use Bitrix\Main\Loader;


$logPath = __DIR__ . "/log";
define("LOG_FILENAME", $logPath . "/" . date("Ymd") . ".log");
file_exists($logPath) || mkdir($logPath, 0777, true);


include_once $_SERVER["DOCUMENT_ROOT"] . "/local/vendor/autoload.php";


/**
 * подключение модуля __projectModule__ (https://bitbucket.org/uplabteam/uplab.core):
 * - модуль сгенерирован автоматически с помощью модуля Uplab.Core
 * - автогенерация констант инфоблоков
 * - автогенерация констант форм
 * - вспомогательные классы для решения часто повторяющихся задач
 * подробнее: https://bitbucket.org/uplabteam/uplab.core
 */
if (!Loader::includeModule("eurocement.local") && !Context::getCurrent()->getRequest()->isAdminSection()) {
	throw new Exception(
		"Необходимо указать в init.php код модуля проекта и убедиться, что модуль сгенерирован и установлен"
	);
}

include_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/wsrubi.smtp/classes/general/wsrubismtp.php");